Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: Apache-DB
Source: https://metacpan.org/release/Apache-DB
Upstream-Contact: Dirk Lindner <lze@cpan.org>

Files: *
Copyright: 1999, Doug MacEachern <dougm@pobox.com>
 2004-2008, Frank Wiles <frank@wiles.org>
License: Artistic or GPL-1+
Comment: The upstream distribution does not contain an explicit statement of
 copyright ownership. Pursuant to the Berne Convention for the Protection of
 Literary and Artistic Works, it is assumed that all content is copyright by
 its respective authors unless otherwise stated.
 Years of copyright taken from uploads to CPAN.

Files: debian/*
Copyright: 2001-2008, Ivan Kohler <ivan-debian@420.am>
 2008-2011, gregor herrmann <gregoa@debian.org>
 2010, Ansgar Burchardt <ansgar@43-1.org>
 2013, Salvatore Bonaccorso <carnil@debian.org>
 2013, Xavier Guimard <yadd@debian.org>
 2019, Joenio Costa <joenio@joenio.me>
License: Artistic or GPL-1+

License: Artistic
 This program is free software; you can redistribute it and/or modify
 it under the terms of the Artistic License, which comes with Perl.
 .
 On Debian systems, the complete text of the Artistic License can be
 found in `/usr/share/common-licenses/Artistic'.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 .
 On Debian systems, the complete text of version 1 of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.
